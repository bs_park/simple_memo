package pif.com.simplememo.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class ToDo (
    @PrimaryKey(autoGenerate = true) val id: Int,
    val content: String,
    val createdDate: String
    )