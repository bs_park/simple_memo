package pif.com.simplememo.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.v4.content.ContextCompat.getSystemService
import android.util.Log
import android.view.inputmethod.InputMethodManager
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import pif.com.simplememo.data.db.AppDB
import pif.com.simplememo.data.entity.ToDo
import pif.com.simplememo.utils.DateUtil


class AddTodoViewModel(application: Application) : AndroidViewModel(application) {
    var todoStr: MutableLiveData<String> = MutableLiveData()


    fun addTodayToDo() {
        val dateUtil = DateUtil()
        val memo = ToDo(0, todoStr.value ?: "", dateUtil.getCurrentDate())

        Observable.just(memo)
            .subscribeOn(Schedulers.io())
            .subscribe({
                AppDB.getInstance(getApplication())!!.getMemoDao().insert(memo)
            }, {
                Log.e("MyTag", it.message)
            })
    }
}
