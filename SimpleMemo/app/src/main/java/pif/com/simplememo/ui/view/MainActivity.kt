package pif.com.simplememo.ui.view

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import pif.com.simplememo.R
import pif.com.simplememo.data.db.AppDB
import pif.com.simplememo.data.entity.ToDo
import pif.com.simplememo.databinding.ActivityMainBinding
import pif.com.simplememo.ui.view.component.ToDoAddTodayDialog
import pif.com.simplememo.ui.view.fragment.AddTodayTodoFragment
import pif.com.simplememo.viewmodel.MainViewModel

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding

    lateinit var fabOpenAnim: Animation
    lateinit var fabCloseAnim: Animation
    private var isFabOpen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        binding.viewModel = mainViewModel

        fabOpenAnim = AnimationUtils.loadAnimation(this, R.anim.fab_open)
        fabCloseAnim = AnimationUtils.loadAnimation(this, R.anim.fab_close)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        binding.navView.setNavigationItemSelectedListener(this)

//        val memo: ToDo = ToDo(0,"memo1", "2018/12/30", "test memo!!!")
//
//        Observable.just(memo)
//            .subscribeOn(Schedulers.io())
//            .subscribe({
//                AppDB.getInstance(this)!!.getMemoDao().insert(memo)
//            }, {
//                Log.e("MyTag", it.message)
//            })


//        var memos: List<ToDo> = ArrayList<ToDo>()
//
//        AppDB.getInstance(this)!!
//            .getMemoDao()
//            .getAllMemo()
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnSubscribe{}
//            .doOnTerminate{}
//            .subscribe({
//                memos =  it.toMutableList()
//            }, {
//
//            })
    }

    fun onFabButtonClick(view: View) {
        var id = view.id

        when(id) {
            R.id.btn_add_action -> {
                println("btn_add_action")
            }
            R.id.btn_add_action_today -> {
                println("btn_add_action_today")

                var fragmentAddToday = AddTodayTodoFragment()

                val transition = supportFragmentManager.beginTransaction()
                transition.add(R.id.todo_today_input_container, fragmentAddToday)
                transition.commit()
//                val addTodayDialog: ToDoAddTodayDialog = ToDoAddTodayDialog()
//                addTodayDialog.show(supportFragmentManager, "todayAdd")

            }
            R.id.btn_add_action_select_calendar -> {
                println("btn_add_action_select_calendar")
            }
        }

        fabAnim()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
                var intent: Intent = Intent(this, SettingActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun fabAnim() {
        if (isFabOpen) {
            binding.btnAddActionToday.startAnimation(fabCloseAnim)
            binding.btnAddActionSelectCalendar.startAnimation(fabCloseAnim)

            binding.btnAddActionToday.isClickable = false
            binding.btnAddActionSelectCalendar.isClickable = false

            isFabOpen = false
        } else {
            binding.btnAddActionToday.startAnimation(fabOpenAnim)
            binding.btnAddActionSelectCalendar.startAnimation(fabOpenAnim)

            binding.btnAddActionToday.isClickable = true
            binding.btnAddActionSelectCalendar.isClickable = true

            isFabOpen = true
        }
    }
}
