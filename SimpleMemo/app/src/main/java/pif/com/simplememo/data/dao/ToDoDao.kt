package pif.com.simplememo.data.dao;

import android.arch.persistence.room.*
import io.reactivex.Flowable
import pif.com.simplememo.data.entity.ToDo

import java.util.List;

@Dao
interface ToDoDao {
    @Query("SELECT * FROM todo")
    fun getAllMemo(): Flowable<List<ToDo>>

    @Query("DELETE FROM todo")
    fun clearAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(todo: ToDo)

    @Update
    fun update(todo: ToDo)

    @Delete
    fun delete(todo: ToDo)
}
