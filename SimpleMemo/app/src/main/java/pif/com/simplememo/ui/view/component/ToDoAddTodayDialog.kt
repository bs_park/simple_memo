package pif.com.simplememo.ui.view.component

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import pif.com.simplememo.R

class ToDoAddTodayDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater? = activity?.layoutInflater

        dialogBuilder.setView(inflater?.inflate(R.layout.dialog_add_today, null))
            .setPositiveButton("ok") { dialog, whichButton ->
            }
            .setNegativeButton("no") { dialog, whichButton ->
                this@ToDoAddTodayDialog.dialog.cancel()
            }

        return dialogBuilder.create()
    }
}