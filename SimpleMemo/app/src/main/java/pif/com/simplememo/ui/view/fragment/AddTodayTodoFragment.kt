package pif.com.simplememo.ui.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager

import pif.com.simplememo.R
import pif.com.simplememo.databinding.FragmentAddTodayTodoBinding
import pif.com.simplememo.viewmodel.AddTodoViewModel


class AddTodayTodoFragment : Fragment() {
    private lateinit var binding: FragmentAddTodayTodoBinding
    private lateinit var viewModel: AddTodoViewModel

//    private var addOkBtnClick: View.OnClickListener = View.OnClickListener {
//        viewModel.addTodayToDo()
//        val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputMethodManager.hideSoftInputFromWindow(binding.inputTodo.windowToken, 0)
//        activity!!.supportFragmentManager.beginTransaction().remove(this).commit()
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_today_todo, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddTodoViewModel::class.java)
        binding.viewModel = viewModel
//        binding.btnAddOk.setOnClickListener(addOkBtnClick)
//        viewModel.addTodayToDo("test")
    }

    fun addOkBtnClick(view: View) {
        viewModel.addTodayToDo()
        val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(binding.inputTodo.windowToken, 0)
        activity!!.supportFragmentManager.beginTransaction().remove(this).commit()
    }
}
