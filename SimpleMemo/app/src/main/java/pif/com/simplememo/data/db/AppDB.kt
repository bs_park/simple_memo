package pif.com.simplememo.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import pif.com.simplememo.data.dao.ToDoDao
import pif.com.simplememo.data.entity.ToDo


@Database(entities = [ToDo::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun getMemoDao(): ToDoDao

    companion object {
        private const val DB_NAME: String = "app.db"
        private var instance: AppDB? = null

        fun getInstance(context: Context): AppDB? {
            if (instance == null) {
                synchronized(AppDB::class) {
                    instance = Room.databaseBuilder(
                        context,
                        AppDB::class.java,
                        DB_NAME
                    ).build()
                }
            }

            return instance
        }
    }
}